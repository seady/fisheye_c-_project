﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projet.NET___Tuiles
{
    public partial class BuildingGridPanel : Panel
    {
        /*private bool fisheye = false;
        private Point focus;
        private Point oldFocus;

        public const int FISHEYE_RANGE = 3;
        public const int FISHEYE_SIZE = FISHEYE_RANGE * Tile.tile_size;

        public void initFisheye()
        { // passer private et insérer dans le constructeur ? (je voulais juste pas toucher au code..)
            MouseMove += _MouseMove;
            MouseEnter += _MouseEnter;
            MouseLeave += _MouseLeave;
            focus = new Point(-1, -1);
            oldFocus = new Point(-1, -1);
            fisheyeOn(); // actif par défaut ?
        }

        private void _MouseLeave(object sender, EventArgs e)
        {
            focus.X = -1;
            focus.Y = -1;
            oldFocus.X = -1;
            oldFocus.Y = -1;
            this.Invalidate();
        }

        private void _MouseEnter(object sender, EventArgs e)
        {
        }

        private void _MouseMove(object sender, MouseEventArgs e)
        {
            if (updateFocus(e))
            {
                //this.Invalidate(getRectFromFocus(oldFocus));
                //this.Invalidate(getRectFromFocus(focus));
                this.Invalidate();
            }
        }

        static private Rectangle getRectFromFocus(Point focus)
        {
            int x, y;
            x = focus.X * Tile.tile_size - FISHEYE_RANGE / 2 * Tile.tile_size;
            y = focus.Y * Tile.tile_size - FISHEYE_RANGE / 2 * Tile.tile_size;
            //if (x < 0) x = 0;
            //if (y < 0) y = 0;
            return new Rectangle(x, y, FISHEYE_SIZE, FISHEYE_SIZE);
        }

        private bool updateFocus(MouseEventArgs e)
        {
            oldFocus.X = focus.X;
            oldFocus.X = focus.Y;
            focus.X = e.X / Tile.tile_size;
            focus.Y = e.Y / Tile.tile_size;
            return ! oldFocus.Equals(focus);
        }

        public void fisheyeOn()
        {
            fisheye = true;
        }
        public void fisheyeOff()
        {
            fisheye = false;
        }
        private void paint()
        {
            Graphics g = CreateGraphics();

            for (int c = 0; c < col; ++c)
            {
                for (int r = 0; r < row; ++r)
                {
                    SolidBrush tileBrush = new SolidBrush(back[c, r].BackColor);
                    g.FillRectangle(tileBrush, new Rectangle(c * Tile.tile_size, r * Tile.tile_size, back[c, r].Width * Tile.tile_size, back[c, r].Height * Tile.tile_size));
                    tileBrush.Dispose();
                }
            }

            if (!fisheye || focus.X == -1)
                return;

            SolidBrush myBrush = new SolidBrush(Color.Red);
            g.FillRectangle(myBrush, getRectFromFocus(focus));
            myBrush.Dispose();
            g.Dispose();
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            paint();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);
            paint();
        }*/
    }
}
