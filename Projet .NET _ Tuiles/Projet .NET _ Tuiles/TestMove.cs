﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projet.NET___Tuiles {
    class TestMove {
        private event FormTestEventHandler formTestEvent;

        private bool m_left_down = false;
        private bool m_right_down = false;

        private bool moving = false;
        private Point start;
        private Point end;

        public TestMove(FormTestEventHandler formTestEvent) {
            this.formTestEvent = formTestEvent;
            init();
        }

        void init() {
            start.X = -1;
            start.Y = -1;
            end.X = -1;
            end.Y = -1;
            m_left_down = false;
            m_right_down = false;
            moving = false;
        }

        public void _MouseUp(object sender, MouseEventArgs e) {
            Control c = (Control)sender;
            if (e.Button == MouseButtons.Right)
                m_right_down = false;
            else if (e.Button == MouseButtons.Left)
                m_left_down = false;
            if(moving && (! m_right_down || ! m_left_down)) {
                moving = false;
                end.X = c.Location.X + e.X;
                end.Y = c.Location.Y + e.Y;
                if(end.X > start.X + 30) {
                    // lancer l'event du special move (fenetre de test)
                    formTestEvent.Invoke();
                }
            }
        }

       public void _MouseDown(object sender, MouseEventArgs e) {
            Control c = (Control)sender;
            if (e.Button == MouseButtons.Right)
                m_right_down = true;
            else if (e.Button == MouseButtons.Left)
                m_left_down = true;
            if (m_right_down && m_left_down) {
                moving = true;
                start.X = c.Location.X + e.X;
                start.Y = c.Location.Y + e.Y;
            }
        }
    }
}
