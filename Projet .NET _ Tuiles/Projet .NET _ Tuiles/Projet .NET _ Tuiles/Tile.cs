﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Projet.NET___Tuiles {

    public partial class Tile : DraggablePanel {
        // Label used to write the associated text
        private Label label;
        public int size_x {
            // Manage width to take 1, 2... tile 
            get { return Width / tile_size; }
            set { Width = value * tile_size; }
        }
        public int size_y {
            // Manage height to take 1, 2... tile 
            get { return Height / tile_size; }
            set { Height = value * tile_size; }
        }

        // Size of a tile (px)
        public const int tile_size = 75;
        public string text {
            // Manage Label's text
            get { return label.Text; }
            set { label.Text = value; }
        }


        public Tile(string text = "", MouseEventHandler meh = null) : base(meh) {
            init();
            size_x = 1;
            size_y = 1;
            ForeColor = SystemColors.ActiveCaptionText;

            this.text = text;
        }


        public Tile(Tile t) : base(t) {
            init();
            Width = t.Width;
            Height = t.Height;

            ForeColor = t.ForeColor;
            BackColor = t.BackColor;
            BorderStyle = t.BorderStyle;

            text = t.text;
        }

        public void init() {
            InitializeComponent();
            label = new Label();
            Controls.Add(label);
        }
    }
}
