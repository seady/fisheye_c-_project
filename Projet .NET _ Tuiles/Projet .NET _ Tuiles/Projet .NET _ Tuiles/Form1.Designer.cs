﻿namespace Projet.NET___Tuiles
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.building_panel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_colrow = new System.Windows.Forms.Label();
            this.number_input_row = new System.Windows.Forms.NumericUpDown();
            this.number_input_col = new System.Windows.Forms.NumericUpDown();
            this.lbl_size = new System.Windows.Forms.Label();
            this.select_size = new System.Windows.Forms.ComboBox();
            this.text_input = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_color = new System.Windows.Forms.Label();
            this.box_see = new System.Windows.Forms.GroupBox();
            this.working_panel = new System.Windows.Forms.Panel();
            this.btn_color = new System.Windows.Forms.Button();
            this.btn_load = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.color_dialog = new System.Windows.Forms.ColorDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.number_input_row)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.number_input_col)).BeginInit();
            this.box_see.SuspendLayout();
            this.SuspendLayout();
            // 
            // building_panel
            // 
            this.building_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.building_panel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.building_panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.building_panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.building_panel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.building_panel.Location = new System.Drawing.Point(242, 0);
            this.building_panel.Name = "building_panel";
            this.building_panel.Size = new System.Drawing.Size(533, 458);
            this.building_panel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbl_colrow);
            this.panel1.Controls.Add(this.number_input_row);
            this.panel1.Controls.Add(this.number_input_col);
            this.panel1.Controls.Add(this.lbl_size);
            this.panel1.Controls.Add(this.select_size);
            this.panel1.Controls.Add(this.text_input);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lbl_color);
            this.panel1.Controls.Add(this.box_see);
            this.panel1.Controls.Add(this.btn_color);
            this.panel1.Controls.Add(this.btn_load);
            this.panel1.Controls.Add(this.btn_save);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(224, 458);
            this.panel1.TabIndex = 1;
            // 
            // lbl_colrow
            // 
            this.lbl_colrow.AutoSize = true;
            this.lbl_colrow.Location = new System.Drawing.Point(6, 401);
            this.lbl_colrow.Name = "lbl_colrow";
            this.lbl_colrow.Size = new System.Drawing.Size(88, 13);
            this.lbl_colrow.TabIndex = 11;
            this.lbl_colrow.Text = "Taille de la grille :";
            // 
            // number_input_row
            // 
            this.number_input_row.Location = new System.Drawing.Point(160, 399);
            this.number_input_row.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.number_input_row.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.number_input_row.Name = "number_input_row";
            this.number_input_row.Size = new System.Drawing.Size(59, 20);
            this.number_input_row.TabIndex = 10;
            this.number_input_row.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.number_input_row.ValueChanged += new System.EventHandler(this.updateGridSize);
            // 
            // number_input_col
            // 
            this.number_input_col.Location = new System.Drawing.Point(92, 399);
            this.number_input_col.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.number_input_col.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.number_input_col.Name = "number_input_col";
            this.number_input_col.Size = new System.Drawing.Size(62, 20);
            this.number_input_col.TabIndex = 9;
            this.number_input_col.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.number_input_col.ValueChanged += new System.EventHandler(this.updateGridSize);
            // 
            // lbl_size
            // 
            this.lbl_size.AutoSize = true;
            this.lbl_size.Location = new System.Drawing.Point(6, 243);
            this.lbl_size.Name = "lbl_size";
            this.lbl_size.Size = new System.Drawing.Size(75, 13);
            this.lbl_size.TabIndex = 8;
            this.lbl_size.Text = "Taille de tuile :";
            // 
            // select_size
            // 
            this.select_size.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.select_size.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.select_size.FormattingEnabled = true;
            this.select_size.Location = new System.Drawing.Point(92, 240);
            this.select_size.Name = "select_size";
            this.select_size.Size = new System.Drawing.Size(127, 21);
            this.select_size.TabIndex = 7;
            this.select_size.SelectedIndexChanged += new System.EventHandler(this.select_size_SelectedIndexChanged);
            // 
            // text_input
            // 
            this.text_input.Location = new System.Drawing.Point(92, 213);
            this.text_input.Name = "text_input";
            this.text_input.Size = new System.Drawing.Size(127, 20);
            this.text_input.TabIndex = 6;
            this.text_input.TextChanged += new System.EventHandler(this.text_input_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 216);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Texte :";
            // 
            // lbl_color
            // 
            this.lbl_color.AutoSize = true;
            this.lbl_color.Location = new System.Drawing.Point(6, 187);
            this.lbl_color.Name = "lbl_color";
            this.lbl_color.Size = new System.Drawing.Size(49, 13);
            this.lbl_color.TabIndex = 4;
            this.lbl_color.Text = "Couleur :";
            // 
            // box_see
            // 
            this.box_see.Controls.Add(this.working_panel);
            this.box_see.Dock = System.Windows.Forms.DockStyle.Top;
            this.box_see.Location = new System.Drawing.Point(0, 0);
            this.box_see.Name = "box_see";
            this.box_see.Size = new System.Drawing.Size(222, 176);
            this.box_see.TabIndex = 2;
            this.box_see.TabStop = false;
            this.box_see.Text = "Prévisualisation";
            // 
            // working_panel
            // 
            this.working_panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.working_panel.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.working_panel.Location = new System.Drawing.Point(68, 42);
            this.working_panel.Name = "working_panel";
            this.working_panel.Size = new System.Drawing.Size(75, 75);
            this.working_panel.TabIndex = 0;
            // 
            // btn_color
            // 
            this.btn_color.BackColor = System.Drawing.Color.RoyalBlue;
            this.btn_color.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_color.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_color.Location = new System.Drawing.Point(92, 182);
            this.btn_color.Name = "btn_color";
            this.btn_color.Size = new System.Drawing.Size(127, 23);
            this.btn_color.TabIndex = 3;
            this.btn_color.UseVisualStyleBackColor = false;
            this.btn_color.Click += new System.EventHandler(this.btn_color_Click);
            // 
            // btn_load
            // 
            this.btn_load.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_load.Location = new System.Drawing.Point(-1, 425);
            this.btn_load.Name = "btn_load";
            this.btn_load.Size = new System.Drawing.Size(115, 32);
            this.btn_load.TabIndex = 1;
            this.btn_load.Text = "Charger";
            this.btn_load.UseVisualStyleBackColor = true;
            this.btn_load.Click += new System.EventHandler(this.btn_load_Click);
            // 
            // btn_save
            // 
            this.btn_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_save.Location = new System.Drawing.Point(110, 425);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(113, 32);
            this.btn_save.TabIndex = 0;
            this.btn_save.Text = "Sauvegarder";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(775, 458);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.building_panel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.number_input_row)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.number_input_col)).EndInit();
            this.box_see.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel building_panel;
        private System.Windows.Forms.GroupBox box_see;
        private System.Windows.Forms.Button btn_load;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ColorDialog color_dialog;
        private System.Windows.Forms.Button btn_color;
        private System.Windows.Forms.Panel working_panel;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Label lbl_color;
        private System.Windows.Forms.TextBox text_input;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_size;
        private System.Windows.Forms.ComboBox select_size;
        private System.Windows.Forms.Label lbl_colrow;
        private System.Windows.Forms.NumericUpDown number_input_row;
        private System.Windows.Forms.NumericUpDown number_input_col;
    }
}

