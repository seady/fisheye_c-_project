﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Projet.NET___Tuiles {

    public partial class DraggablePanel : Panel {
        private int x, y;
        private bool drag = false;
        private bool can_be_dragged = true;
        private MouseEventHandler meh;

        public Point defaultLocation;

        public DraggablePanel(MouseEventHandler meh = null) {
            this.meh = meh;
            init();
        }

        public DraggablePanel(DraggablePanel dp) {
            meh = dp.meh;
            init();
        }

        private void init() {
            InitializeComponent();
            MouseDown += _MouseDown;
            if (meh != null) MouseUp += meh;
            MouseUp += _MouseUp;
            MouseMove += _MouseMove;
            defaultLocation = Location;
        }

        private void _MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                drag = true && can_be_dragged;
                x = e.X;
                y = e.Y;
            }
        }

        private void _MouseUp(object sender, MouseEventArgs e) {
            drag = false;
            Location = defaultLocation;
        }

        private void _MouseMove(object sender, MouseEventArgs e) {
            if (drag) this.Location = new Point(e.X + Left - x, e.Y + Top - y);
        }

        public void saveLocationAsDefault() {
            defaultLocation = Location;
        }

        public void stopDrag() {
            can_be_dragged = false;
            drag = false;
        }
    }
}
