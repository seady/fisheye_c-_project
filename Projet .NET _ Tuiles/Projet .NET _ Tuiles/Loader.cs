﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Projet.NET___Tuiles {
    public class Loader {
        private static XmlSerializer serializer = new XmlSerializer(typeof(List<TileData>));

        public static void saveGrid(BuildingGridPanel grid, string file_name) {
            FileStream file = File.Create(file_name);
            serializer.Serialize(file, grid.getTileData());
            file.Close();
        }

        public static List<TileData> loadGrid(string file_name) {
            StreamReader file = new StreamReader(file_name);
            List<TileData> list = (List<TileData>)serializer.Deserialize(file);
            file.Close();
            return list;
        }
    }
}
