﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projet.NET___Tuiles {
    public partial class Form2 : Form {

        private int col, row;
        private TileData [,] tileTab;
        Timer timer;

        public Form2(int col, int row, List<TileData> tileData) {
            InitializeComponent();
            this.col = col;
            this.row = row;
            tileTab = new TileData[col, row];
            for (int i = 0; i < tileData.Count; ++i) {
                TileData data = tileData.ElementAt(i);
                tileTab[data.x, data.y] = new TileData(data);
            }
            timer = new Timer();
            timer.Interval = 15;
            timer.Tick += _Tick;
            timer.Enabled = true;
            timer.Start();
        }

        private void _Tick(Object myObject, EventArgs myEventArgs) {
            Invalidate();
        }

        private void paint() {
            Graphics g = CreateGraphics();


            // TODO : DEBUG
            for (int c = 0; c < col; ++c) {
                for (int r = 0; r < row; ++r) {
                    if (tileTab[c, r] != null) {
                        SolidBrush tileBrush = new SolidBrush(ColorTranslator.FromHtml(tileTab[c, r].color));
                        g.FillRectangle(tileBrush, new Rectangle(c * Tile.tile_size, r * Tile.tile_size, tileTab[c, r].size_x * Tile.tile_size, tileTab[c, r].size_y * Tile.tile_size));
                        tileBrush.Dispose();
                    }
                }
            }

            // ceci fonctionne :
            /*SolidBrush myBrush = new SolidBrush(Color.Red);
            g.FillRectangle(myBrush, new Rectangle(0, 0, 200, 200));
            myBrush.Dispose();*/

            g.Dispose();
        }

        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);
            paint();
        }

        protected override void OnPaintBackground(PaintEventArgs e) {
            base.OnPaintBackground(e);
            paint();
        }
    }
}
