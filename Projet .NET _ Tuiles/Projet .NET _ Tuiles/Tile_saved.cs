﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet.NET___Tuiles {
    public class TileData {
        public int size_x;
        public int size_y;

        public string color;
        public string text;

        public int x;
        public int y;

        public TileData() {
            this.size_x = 1;
            this.size_y = 1;
            this.x = 0;
            this.y = 0;
            color = "";
            text = "";
        }

        public TileData(TileData td) {
            size_x = td.size_x;
            size_y = td.size_y;
            color = td.color;
            text = td.text;
            x = td.x;
            y = td.y;
        }

        public TileData(Tile t) {
            size_x = t.size_x;
            size_y = t.size_y;
            x = t.Left / Tile.tile_size;
            y = t.Top / Tile.tile_size;
            color = ColorTranslator.ToHtml(t.BackColor);
            text = t.text;
        }
    }
}
