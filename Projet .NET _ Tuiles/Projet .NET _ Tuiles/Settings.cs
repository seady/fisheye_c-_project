﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.IO;
using System.Drawing;

namespace Projet.NET___Tuiles {
    public class Settings {
        public static XmlSerializer serializer = new XmlSerializer(typeof(Settings));
        private static string file_name = Environment.CurrentDirectory + "/settings/settings.xml";


        public string[] formats = new string[1] { "1x1" };

        [XmlIgnore]
        public int grid_size_col = 6;
        [XmlIgnore]
        public int grid_size_row = 5;

        [XmlElement(ElementName = "Name")]
        public string name_tile { get; set; }

        [XmlElement(ElementName = "FormatOfTile")]
        public string size_tile { get; set; }

        [XmlElement(ElementName = "SizeOfCol")]
        public int size_col { get; set; }

        [XmlElement(ElementName = "SizeOfRow")]
        public int size_row { get; set; }

        [XmlElement(ElementName = "Color")]
        public string tilecolor { get; set; }

        /* [XmlElement(ElementName = "ColorOfTile")]
         public string panelcolor { get; set; }*/







        /*public string[] Formats
        {
            get { return formats; }
            set { formats = value; }
        }*/
        /* public int Grid_Size_Col
         {
             get { return grid_size_col; }
             set { grid_size_col = value; }
         }
         public int Grid_Size_Row
         {
             get { return grid_size_row; }
             set { grid_size_row = value; }

         }*/

        public static Settings loadSettings() {
            StreamReader file = new StreamReader(file_name);
            Settings settings = (Settings)serializer.Deserialize(file);
            file.Close();
            return settings;
        }

        public static void SaveSettings(Object Iclass, string filename) {
            StreamWriter writer = null;
            try {
                XmlSerializer serializer = new XmlSerializer(Iclass.GetType());
                writer = new StreamWriter(filename);
                serializer.Serialize(writer, Iclass);
            } finally {
                if (writer != null)
                    writer.Close();
                writer = null;
            }
        }
        /* public static void SaveSettings(Settings settings) {
             FileStream file = File.Create(file_name);

             serializer.Serialize(file, settings);
             file.Close();
         }*/
    }
}
