﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Projet.NET___Tuiles {

    public partial class Form1 : Form {
        private Tile draggable_panel;
        private BuildingGridPanel building_grid_panel;
        private Settings settings;
        // DataSet dt = new DataSet();
        //private SaveXml save_xml;

        public Form1() {
            InitializeComponent();
            settings = Settings.loadSettings();

            building_grid_panel = new BuildingGridPanel(settings.grid_size_col, settings.grid_size_row);
            building_panel.Controls.Add(building_grid_panel);
            centerElement(working_panel);
            createTempPanel();

            select_size.Items.Clear();
            foreach (string size in settings.formats) {
                select_size.Items.Add(size);
            }
        }

        public void centerElement(Control element, Control parent = null) {
            if (parent == null) parent = element.Parent;
            element.Left = (parent.Width - element.Width) / 2;
            element.Top = (parent.Height - element.Height) / 2;
        }

        public Point getAbsoluteLocation(Control element) {
            Point loc = element.Location;
            Control temp = element.Parent;
            while (temp.Parent != null) {
                loc.X += temp.Location.X;
                loc.Y += temp.Location.Y;
                temp = temp.Parent;
            }
            return loc;
        }

        public Color selectColor() {
            DialogResult result = color_dialog.ShowDialog();
            if (result == DialogResult.OK) return color_dialog.Color;
            else return draggable_panel.BackColor;
        }

        private void btn_color_Click(object sender, EventArgs e) {
            draggable_panel.BackColor = selectColor();
            btn_color.BackColor = draggable_panel.BackColor;
        }

        private void createTempPanel() {
            draggable_panel = new Tile("", (object sender, MouseEventArgs e) => DraggablePanelMouseUp((Tile)sender));
            draggable_panel.BackColor = btn_color.BackColor;
            draggable_panel.Left = working_panel.Left;
            draggable_panel.Top = working_panel.Top;
            draggable_panel.saveLocationAsDefault();
            draggable_panel.BorderStyle = BorderStyle.FixedSingle;

            Controls.Add(draggable_panel);
            draggable_panel.BringToFront();
        }

        private void text_input_TextChanged(object sender, EventArgs e) {
            draggable_panel.text = text_input.Text;
        }

        private void select_size_SelectedIndexChanged(object sender, EventArgs e) {
            string[] s = select_size.Text.Split('x');

            if (s.Length == 2) {
                draggable_panel.size_x = int.Parse(s[0]);
                draggable_panel.size_y = int.Parse(s[1]);

                centerElement(draggable_panel, box_see);
                draggable_panel.saveLocationAsDefault();
            }
        }

        /* Added to the draggable_panel and used on all tiles placed */
        public void DraggablePanelMouseUp(object o) {
            Tile dragged_tile = (Tile)o;

            Point drag_loc = getAbsoluteLocation(dragged_tile);
            Point building_grid_panel_loc = getAbsoluteLocation(building_grid_panel);

            int col = (drag_loc.X - building_grid_panel_loc.X + Tile.tile_size / 2) / Tile.tile_size,
                row = (drag_loc.Y - building_grid_panel_loc.Y + Tile.tile_size / 2) / Tile.tile_size;

            if (dragged_tile.Parent == building_grid_panel) {
                // A tile were moved
                Point t = dragged_tile.defaultLocation;
                t.X = t.X / Tile.tile_size;
                t.Y = t.Y / Tile.tile_size;

                building_grid_panel.moveTile(t.X, t.Y, col, row);
            } else {
                // draggable_panel is dragged, we need to copy it
                Tile dragged_tile_copy = new Tile(dragged_tile);
                dragged_tile_copy.MouseClick += _MouseClick;
                building_grid_panel.addTile(col, row, dragged_tile_copy);
            }
        }

        private void _MouseClick(object sender, MouseEventArgs e) {
            Tile t = (Tile)sender;
            if (e.Button == MouseButtons.Right) {
                draggable_panel.BackColor = t.BackColor;
                btn_color.BackColor = t.BackColor;
                draggable_panel.text = t.text;
                text_input.Text = t.text;
                draggable_panel.size_x = t.size_x;
                draggable_panel.size_y = t.size_y;
                select_size.Text = t.size_x + "x" + t.size_y;
                centerElement(draggable_panel, box_see);
            }
        }

        private void updateGridSize(object sender, EventArgs e) {
            building_grid_panel.ResizeColRow((int)(number_input_col.Value), (int)(number_input_row.Value));
        }

        // saving data flow in a xml file'directory
        private void btn_save_Click(object sender, EventArgs e) {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Xml Files(*.xml)|*.xml";
            save.FilterIndex = 8;
            save.RestoreDirectory = true;
            save.InitialDirectory = Environment.CurrentDirectory + "\\settings";
            if (save.ShowDialog() == DialogResult.OK) {
                try {
                    Loader.saveGrid(building_grid_panel, save.FileName);

                    /*
                    //SaveTile svt = new SaveTile();
                    settings.name_tile = text_input.Text;
                    settings.size_tile = select_size.SelectedItem.ToString();
                    settings.size_col = (int)number_input_col.Value;
                    settings.size_row = (int)number_input_row.Value;
                    settings.tilecolor = btn_color.BackColor.Name.ToString();
                    //settings.panelcolor = btn_color.BackColor.Name.ToString();
                    //settings.tilecolor = btn_color.BackColor.ToString();

                    //svt.TileColor = Convert.ToInt32(btn_color.Text);
                    // SaveXml.Save_Tile_Data(save_xml, save.FileNa);
                    Settings.SaveSettings(settings, save.FileName);
                    */
                } catch (Exception ex) {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        // loading a xml file on the screen
        private void btn_load_Click(object sender, EventArgs e) {
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "Xml Files(*.xml)|*.xml";
            od.RestoreDirectory = true;
            od.InitialDirectory = Environment.CurrentDirectory + "\\settings";
            if (od.ShowDialog() == DialogResult.OK) {
                try {
                    List<TileData> ts = Loader.loadGrid(od.FileName);
                    building_grid_panel.ClearGrid();
                    ts.ForEach(delegate (TileData t) {
                        building_grid_panel.addTile(t.x, t.y, new Tile(t));
                    });

                    /* XmlSerializer xs = new XmlSerializer(typeof(Settings));

                    /XmlReader reader = XmlReader.Create(od.FileName, new XmlReaderSettings());
                    Settings settings = (Settings)xs.Deserialize(reader);
                    text_input.Text = settings.name_tile;
                    select_size.Text = settings.size_tile;
                    number_input_col.Value = settings.size_col;
                    number_input_row.Value = settings.size_row;
                    btn_color.BackColor = Color.FromName(settings.tilecolor);
                    draggable_panel.BackColor = btn_color.BackColor;
                    working_panel.BackColor = draggable_panel.BackColor; */
                } catch (Exception ex) {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}

