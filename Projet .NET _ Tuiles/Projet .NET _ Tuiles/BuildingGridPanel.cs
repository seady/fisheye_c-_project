﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projet.NET___Tuiles {
    public delegate void FormTestEventHandler();

    public partial class BuildingGridPanel : Panel {
        private int col, row;
        private Tile[,] back;
        private Tile[,] working;
        private bool[,] free_tiles;

        private TestMove testMove;

        private event FormTestEventHandler formTestEvent;

        public void startTestForm() {
            new Form2(col, row, getTileData()).ShowDialog();
        }

        public List<TileData> getTileData() {
            List<TileData> list = new List<TileData>();
            for (int i = 0; i < working.GetLength(0); i++) {
                for (int j = 0; j < working.GetLength(1); j++) {
                    if (working[i, j] != null) {
                        list.Add(working[i, j].getData());
                    }
                }
            }
            return list;
        }

        public BuildingGridPanel(int col = 2, int row = 2) {
            formTestEvent += new FormTestEventHandler(startTestForm);
            testMove = new TestMove(formTestEvent);
            InitializeComponent();
            this.col = col;
            this.row = row;
            back = new Tile[col, row];
            working = new Tile[col, row];
            free_tiles = new bool[col, row];
            Width = col * Tile.tile_size;
            Height = row * Tile.tile_size;
            CreateGrid();
        }

        public void ResizeColRow(int col, int row) {
            int old_col = this.col, old_row = this.row;
            this.col = col;
            this.row = row;
            Width = col * Tile.tile_size;
            Height = row * Tile.tile_size;
            Tile[,] new_back = new Tile[col, row];
            Tile[,] new_working = new Tile[col, row];
            bool[,] new_free = new bool[col, row];

            for (int i = 0; i < Math.Max(col, old_col); i++) {
                for (int j = 0; j < Math.Max(row, old_row); j++) {
                    // Delete elements out of bounds
                    if (i < working.GetLength(0) && j < working.GetLength(1) && working[i, j] != null && !checkColRow(i, j, working[i, j].size_x, working[i, j].size_y)) {
                        removeTile(i, j);
                    }

                    if (i < Math.Min(col, old_col) && j < Math.Min(row, old_row)) {
                        // We just copy settings
                        new_back[i, j] = back[i, j];
                        new_working[i, j] = working[i, j];
                        new_free[i, j] = free_tiles[i, j];
                    } else if (i >= old_col || j >= old_row) {
                        // We need to create new slots
                        new_working[i, j] = null;
                        new_free[i, j] = true;
                        new_back[i, j] = createBackTile(i, j);
                        addControl(new_back[i, j]);
                    } else {
                        // Slot is no longer displayed
                        Controls.Remove(back[i, j]);
                    }
                }
            }
            back = new_back;
            working = new_working;
            free_tiles = new_free;

        }

        public void CreateGrid() {
            for (int c = 0; c < col; c++) {
                for (int r = 0; r < row; r++) {
                    working[c, r] = null;
                    free_tiles[c, r] = true;
                    back[c, r] = createBackTile(c, r);
                    addControl(back[c, r]);
                }
            }
        }

        public void ClearGrid() {
            for (int c = 0; c < col; c++) {
                for (int r = 0; r < row; r++) {
                    if (working[c,r] != null) removeTile(c, r);
                }
            }
        }

        public Tile createBackTile(int col, int row) {
            Tile t = new Tile(col + "|" + row);
            if ((row * this.row + col) % 2 == 0) t.BackColor = SystemColors.GradientInactiveCaption;
            else t.BackColor = SystemColors.GradientActiveCaption;
            t.Left = col * Tile.tile_size;
            t.Top = row * Tile.tile_size;
            t.saveLocationAsDefault();
            t.BorderStyle = BorderStyle.FixedSingle;
            t.stopDrag();
            return t;
        }

        // Add a tile
        public bool addTile(int col, int row, Tile tile, bool new_tile = true) {
            Console.WriteLine(col + "-" + row);

            if (checkColRow(col, row, tile.size_x, tile.size_y)) {
                bool free = true;
                for (int i = 0; i < tile.size_x; i++) for (int j = 0; j < tile.size_y; j++) {
                        free &= (free_tiles[col + i, row + j]);
                }
                if (free) {
                    // Set to false used tiles
                    for (int i = 0; i < tile.size_x; i++) for (int j = 0; j < tile.size_y; j++) {
                            free_tiles[col + i, row + j] = false;
                            Controls.Remove(back[col + i, row + j]);
                            Console.WriteLine(col + "-" + row);
                    }
                    working[col, row] = tile;

                    //if (new_tile) working[col, row].MouseClick += _MouseClick;
                    addControl(working[col, row], new_tile);
                    working[col, row].Top = row * Tile.tile_size;
                    working[col, row].Left = col * Tile.tile_size;
                    working[col, row].saveLocationAsDefault();
                    working[col, row].BringToFront();
                    return true;
                }
            }
            return false;
        }

        private void addControl(Control c, bool new_c = true) {
            Controls.Add(c);
            if (!new_c)
                return;
            c.MouseUp += testMove._MouseUp;
            c.MouseDown += testMove._MouseDown;
        }

        // Remove a tile
        public Tile removeTile(int col, int row) {
            Tile tile = working[col, row];
            // Set to true tiles free
            for (int i = 0; i < tile.size_x; i++) for (int j = 0; j < tile.size_y; j++) {
                    free_tiles[col + i, row + j] = true;
                    addControl(back[col + i, row + j]);
                }
            working[col, row] = null;
            Controls.Remove(tile);
            addControl(back[col, row]);
            return tile;
        }

        // Move a tile to new coords
        public void moveTile(int pred_col, int pred_row, int col, int row) {
            Tile tile = removeTile(pred_col, pred_row);
            if (checkColRow(col, row)) {
                // col and row are in
                if (!addTile(col, row, tile)) {
                    addTile(pred_col, pred_row, tile, false);
                }
            } // Else, we just remove the tile
        }

        // Check if a tile will not be out of bounds
        public bool checkColRow(int col, int row, int size_col = 1, int size_row = 1) {
            size_col--;
            size_row--;
            return ((col + size_col) < this.col && (row + size_row) < this.row && col >= 0 && row >= 0);
        }
    }
}
