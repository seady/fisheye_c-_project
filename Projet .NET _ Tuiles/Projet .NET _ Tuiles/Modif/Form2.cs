﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projet.NET___Tuiles {
    public partial class Form2 : Form {

        private int col, row;
        private TileData [,] tileTab;
        private List<TileData> tileData;

        public Form2(int col, int row, List<TileData> tileData) {
            InitializeComponent();
            this.col = col;
            this.row = row;
            this.tileData = tileData;
            tileTab = new TileData[col, row];
            for (int i = 0; i < tileData.Count; ++i) {
                TileData data = tileData.ElementAt(i);
                tileTab[data.x, data.y] = new TileData(data);
            }
            initFisheye();
        }

        private void paint() {
            // mise en place du double buffering :
            BufferedGraphicsContext currentContext;
            BufferedGraphics myBuffer; // backbuffer
            currentContext = BufferedGraphicsManager.Current;
            myBuffer = currentContext.Allocate(this.CreateGraphics(), this.DisplayRectangle);

            Graphics g = myBuffer.Graphics; // on va dessiner sur le backbuffer et le flip avec le frontbuf (buffer d'affichage) à la fin
            Pen pen = new Pen(Color.DarkGray, 1);
            SolidBrush whiteBrush = new SolidBrush(Color.White);

            g.FillRectangle(whiteBrush, this.DisplayRectangle);
            g.DrawRectangle(pen, new Rectangle(0, 0, col * Tile.tile_size, row * Tile.tile_size));

            paintFisheye(g);

            myBuffer.Render(this.CreateGraphics()); // flip avec le frontbuf

            pen.Dispose();
            whiteBrush.Dispose();
            myBuffer.Dispose();
        }

        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);
            paint();
        }

        protected override void OnPaintBackground(PaintEventArgs e) {
            base.OnPaintBackground(e);
            paint();
        }
    }
}
