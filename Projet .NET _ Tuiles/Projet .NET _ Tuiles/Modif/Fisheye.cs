﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projet.NET___Tuiles {
    public partial class Form2 : Form {
        private bool fisheye = false;
        private bool outside = true;
        private Point focus;
        private Point oldFocus;

        public const int FISHEYE_RANGE = 5;
        public const int FISHEYE_SIZE = FISHEYE_RANGE * Tile.tile_size;

        public void initFisheye() {
            MouseMove += _MouseMove;
            MouseEnter += _MouseEnter;
            MouseLeave += _MouseLeave;
            outside = true;
            focus = new Point(-1, -1);
            oldFocus = new Point(-1, -1);
            fisheyeOn(); // actif par défaut ?
        }

        private bool isOutside(MouseEventArgs e) {
            return e.X >= Tile.tile_size * col || e.Y >= Tile.tile_size * row;
        }

        private void _MouseLeave(object sender, EventArgs e) {
            focus.X = -1;
            focus.Y = -1;
            oldFocus.X = -1;
            oldFocus.Y = -1;
            this.Invalidate();
        }

        private void _MouseEnter(object sender, EventArgs e) {
        }

        private void _MouseMove(object sender, MouseEventArgs e) {
            if(isOutside(e)) {
                if (!outside) {
                    outside = true;
                    _MouseLeave(sender, e);
                }
                return;
            }
            if(! isOutside(e) && outside) {
                outside = false;
            }
            if (updateFocus(e)) {
                paint();
            }
        }

        static private Rectangle getRectFromFocus(Point focus) {
            int x, y;
            x = focus.X * Tile.tile_size - FISHEYE_RANGE / 2 * Tile.tile_size;
            y = focus.Y * Tile.tile_size - FISHEYE_RANGE / 2 * Tile.tile_size;
            return new Rectangle(x, y, FISHEYE_SIZE, FISHEYE_SIZE);
        }

        private bool updateFocus(MouseEventArgs e) {
            oldFocus.X = focus.X;
            oldFocus.X = focus.Y;
            focus.X = e.X / Tile.tile_size;
            focus.Y = e.Y / Tile.tile_size;
            return ! oldFocus.Equals(focus);
        }

        public void fisheyeOn() {
            fisheye = true;
        }
        public void fisheyeOff() {
            fisheye = false;
        }
        public static double FDeform(double x) { // fonction FDeform de Frédéric Vernier
            double r = FISHEYE_RANGE * Tile.tile_size / 2;
            double z = 100;
            r += z;
            if (x >= r - z) // x = r-z => FDeform(x) = x  On ne doit donc pas appliquer de déformation pour les valeurs supérieures à ce x
                return x;
            return (r * z) / (-z - x) + r;
        }
        public Point applyFisheye(Point p, Point center) {
            if (!fisheye || focus.X == -1)
                return p;
            double dist = Math.Sqrt((p.X - center.X) * (p.X - center.X) + (p.Y - center.Y) * (p.Y - center.Y));
            if (dist == 0)
                return p;
            double scale = FDeform(dist) / dist;
            p.X = (int)(center.X + (p.X - center.X) * scale + 0.5);
            p.Y = (int)(center.Y + (p.Y - center.Y) * scale + 0.5);
            return p;
        }
        private TileData getTileFromFocus() {
            TileData ret = null;
            int x = focus.X;
            int y = focus.Y;
            if (x < 0 || y < 0)
                return null;
            tileData.ForEach(delegate (TileData t) {
                if (t.x <= x && t.x + t.size_x * Tile.tile_size - 1 >= x && t.y <= y && t.y + t.size_y * Tile.tile_size - 1 >= y)
                    ret = t;
            });
            return ret;
        }
        private Point getCenterFromFocus() {
            Rectangle fishRect = getRectFromFocus(focus);
            Point center = new Point(fishRect.X + FISHEYE_RANGE * Tile.tile_size / 2, fishRect.Y + FISHEYE_RANGE * Tile.tile_size / 2);
            TileData t = getTileFromFocus();
            if (t == null)
                return center;
            return new Point((t.x * Tile.tile_size + (t.size_x * Tile.tile_size / 2)), (t.y * Tile.tile_size + (t.size_y * Tile.tile_size / 2)));
        }
        private void paintFisheye(Graphics g) {
            Point center = getCenterFromFocus();

            Point[] points = new Point[4];
            Pen pen = new Pen(Color.DarkGray, 1);
            FontFamily fontFamily = new FontFamily("Arial");
            Font font = new Font(fontFamily, 12, FontStyle.Regular, GraphicsUnit.Pixel);
            SolidBrush blackBrush = new SolidBrush(Color.Black);
            for (int c = 0; c < col; ++c) {
                for (int r = 0; r < row; ++r) {
                    if (tileTab[c, r] != null) {
                        TileData t = tileTab[c, r];
                        int x = t.x * Tile.tile_size;
                        int y = t.y * Tile.tile_size;
                        int offsetx = t.size_x * Tile.tile_size;
                        int offsety = t.size_y * Tile.tile_size;
                        points[0] = applyFisheye(new Point(x, y), center);
                        points[1] = applyFisheye(new Point(x + offsetx, y), center);
                        points[2] = applyFisheye(new Point(x + offsetx, y + offsety), center);
                        points[3] = applyFisheye(new Point(x, y + offsety), center);
                        SolidBrush tileBrush = new SolidBrush(ColorTranslator.FromHtml(t.color));
                        g.FillPolygon(tileBrush, points);
                        g.DrawString(t.text, font, blackBrush, points[0].X + 2, points[0].Y + 2);
                        g.DrawPolygon(pen, points);
                        tileBrush.Dispose();
                    }
                }
            }

            pen.Dispose();
            fontFamily.Dispose();
            font.Dispose();
            blackBrush.Dispose();
        }
    }
}
